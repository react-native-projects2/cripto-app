import React, { useEffect, useState } from "react";
import {
  Alert,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
} from "react-native";

import { Picker } from "@react-native-picker/picker";
import { fetchListCripto, fetchPriceCripto } from "../utils/fetchApi";

import * as Font from "expo-font";
import AppLoading from "expo-app-loading";

const loadFonts = () => {
  return Font.loadAsync({
    "gotham-bold": require("../../assets/fonts/GothamRounded-Bold.otf"),
    "gotham-reg": require("../../assets/fonts/GothamRounded-Book.otf"),
    "gotham-med": require("../../assets/fonts/GothamRounded-Medium.otf"),
  });
};

const Form = ({ setCriptoInfo, setLoading }) => {
  const [fontLoaded, setFontLoaded] = useState(false);

  const [coin, setCoin] = useState("");
  const [cripto, setCripto] = useState("");
  const [criptoList, setCriptoList] = useState([]);

  useEffect(() => {
    (async () => {
      const data = await fetchListCripto();
      setCriptoList(data);
    })();
  }, []);

  const getPriceCripto = async () => {
    if (coin.trim() === "" || cripto.trim() === "") {
      showAlert();
      return;
    }
    setLoading(true);

    const dataCripto = await fetchPriceCripto(coin, cripto);
    setCriptoInfo(dataCripto);

    setLoading(false);
  };

  if (!fontLoaded) {
    return (
      <AppLoading
        startAsync={loadFonts}
        onError={(err) => console.error("Error", err)}
        onFinish={() => setFontLoaded(true)}
      />
    );
  }

  const showAlert = () => {
    Alert.alert(
      "Error", // Titulo
      "Debe seleccionar una moneda y una criptomoneda.", // Texto del alert
      [
        {
          // array de botones
          text: "OK",
        },
      ]
    );
  };

  return (
    <>
      <View style={styles.form}>
        <View style={styles.formSection}>
          <Text style={styles.label}>Moneda</Text>
          <Picker
            selectedValue={coin}
            onValueChange={(itemValue) => setCoin(itemValue)}
          >
            <Picker.Item label="- Selecciona una moneda -" value="" />
            <Picker.Item label="Dólar Americano" value="USD" />
            <Picker.Item label="Euro" value="EUR" />
            <Picker.Item label="Libra Esterlina" value="GBP" />
            <Picker.Item label="Peso Argentino" value="ARS" />
          </Picker>
        </View>

        <View style={styles.formSection}>
          <Text style={styles.label}>Criptomoneda</Text>
          <Picker
            selectedValue={cripto}
            onValueChange={(itemValue) => setCripto(itemValue)}
          >
            <Picker.Item label="- Selecciona una Cripto -" value="" />
            {criptoList.map(({ CoinInfo }) => {
              return (
                <Picker.Item
                  label={CoinInfo.FullName}
                  value={CoinInfo.Name}
                  key={CoinInfo.Id}
                />
              );
            })}
          </Picker>
        </View>
      </View>

      <TouchableHighlight style={styles.button} onPress={getPriceCripto}>
        <Text style={styles.textButton}>Cotizar</Text>
      </TouchableHighlight>
    </>
  );
};

const styles = StyleSheet.create({
  form: {
    marginTop: 30,
  },
  formSection: {
    marginBottom: 30,
  },
  label: {
    color: "#0f0623",
    fontSize: 18,
    fontWeight: "700",
    textTransform: "uppercase",
  },
  button: {
    backgroundColor: "#00dd73",
    borderRadius: 8,
    marginTop: 60,
    paddingVertical: 20,
  },
  textButton: {
    color: "#ffffff",
    fontFamily: "gotham-bold",
    fontSize: 16,
    textAlign: "center",
    textTransform: "uppercase",
  },
});

export default Form;
