import React, { useState } from "react";
import { Image, StyleSheet, Text, View } from "react-native";

import * as Font from "expo-font";
import AppLoading from "expo-app-loading";

const loadFonts = () => {
  return Font.loadAsync({
    "gotham-bold": require("../../assets/fonts/GothamRounded-Bold.otf"),
    "gotham-reg": require("../../assets/fonts/GothamRounded-Book.otf"),
    "gotham-med": require("../../assets/fonts/GothamRounded-Medium.otf"),
  });
};

const CriptoInfo = ({ criptoInfo }) => {
  const {
    PRICE,
    LASTUPDATE,
    IMAGEURL,
    OPENDAY,
    HIGHDAY,
    LOWDAY,
    CHANGEPCTDAY,
  } = criptoInfo;

  const [fontLoaded, setFontLoaded] = useState(false);

  if (!fontLoaded) {
    return (
      <AppLoading
        startAsync={loadFonts}
        onError={(err) => console.error("Error", err)}
        onFinish={() => setFontLoaded(true)}
      />
    );
  }

  return (
    <View style={styles.infoWrapper}>
      <Image
        style={styles.tinyLogo}
        source={{ uri: `https://www.cryptocompare.com${IMAGEURL}` }}
      />
      <Text style={styles.titlePrice}>{PRICE}</Text>
      <Text style={styles.subtitleUpdate}>
        Update:
        <Text style={styles.strongUpdate}>{LASTUPDATE}</Text>
      </Text>
      <View style={styles.moreInfo}>
        <View style={styles.item}>
          <Text style={styles.itemLabel}>Price variation %</Text>
          <Text style={styles.itemText}>{CHANGEPCTDAY} %</Text>
        </View>
        <View style={styles.item}>
          <Text style={styles.itemLabel}>Open Day</Text>
          <Text style={styles.itemText}>{OPENDAY}</Text>
        </View>
        <View style={styles.item}>
          <Text style={styles.itemLabel}>High Day</Text>
          <Text style={styles.itemText}>{HIGHDAY}</Text>
        </View>
        <View style={styles.item}>
          <Text style={styles.itemLabel}>Low Day</Text>
          <Text style={styles.itemText}>{LOWDAY}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  infoWrapper: {
    alignItems: "center",
    backgroundColor: "#f7f7f7",
    marginVertical: 25,
    paddingVertical: 25,
  },
  tinyLogo: {
    height: 50,
    marginBottom: 20,
    width: 50,
  },
  titlePrice: {
    color: "#0f0623",
    fontFamily: "gotham-bold",
    fontSize: 32,
    textAlign: "center",
  },
  subtitleUpdate: {
    color: "#7f7f7f",
    fontFamily: "gotham-reg",
    fontSize: 12,
    marginTop: 10,
    textTransform: "uppercase",
  },
  strongUpdate: {
    fontFamily: "gotham-bold",
  },
  moreInfo: {
    marginVertical: 50,
    paddingHorizontal: 25,
  },
  item: {
    borderTopColor: "#cbcbcb",
    borderTopWidth: 1,
    flexDirection: "row",
    paddingHorizontal: 2,
    paddingVertical: 20,
    width: "100%",
  },
  itemLabel: {
    flex: 1,
    fontFamily: "gotham-reg",
    fontSize: 12,
    textTransform: "uppercase",
  },
  itemText: {
    flex: 1,
    fontFamily: "gotham-med",
    fontSize: 14,
    textAlign: "right",
  },
});

export default CriptoInfo;
