import React, { useState } from 'react';
import { Platform, StyleSheet, Text } from "react-native";

import * as Font from 'expo-font';
import AppLoading from 'expo-app-loading';

const loadFonts = () => {
    return Font.loadAsync({
        "gotham-bold": require("../../assets/fonts/GothamRounded-Bold.otf"),
        "gotham-reg": require("../../assets/fonts/GothamRounded-Book.otf"),
        "gotham-med": require("../../assets/fonts/GothamRounded-Medium.otf"),
    })
};

const Header = () => {

    const [fontLoaded, setFontLoaded] = useState(false);

    if(!fontLoaded) {
        return <AppLoading
            startAsync={ loadFonts }
            onError={ err => console.error('Error', err) }
            onFinish={() => setFontLoaded(true)}
        />
    }

    return(

        <Text style={ styles.header }>Cripto App</Text>
    )
};

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#0f0623',
        color: '#ffffff',
        fontFamily: 'gotham-bold',
        fontSize: 18,
        marginBottom: 30,
        paddingBottom: 20,
        paddingTop: Platform.OS === 'ios' ? 50 : 40,
        textAlign: 'center',
        textTransform: 'uppercase',
    },  
});

export default Header;
