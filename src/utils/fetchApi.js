import axios from "axios";

export const fetchListCripto = async () => {
  const url = `https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD`;
  const { data } = await axios.get(url);
  return data.Data;
};

export const fetchPriceCripto = async (coin = "USD", cripto = "BTC") => {
  const url = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${cripto}&tsyms=${coin}`;
  const { data } = await axios.get(url);
  return data.DISPLAY[cripto][coin];
};
