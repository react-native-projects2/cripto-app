import * as Font from 'expo-font';

export const loadFonts = () => {
    return Font.loadAsync({
        "gotham-bold": require("../../assets/fonts/GothamRounded-Bold.otf"),
        "gotham-reg": require("../../assets/fonts/GothamRounded-Book.otf"),
        "gotham-med": require("../../assets/fonts/GothamRounded-Medium.otf"),
    })
};