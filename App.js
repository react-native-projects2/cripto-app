import React, { useState } from "react";
import { ActivityIndicator, StyleSheet, ScrollView, View } from "react-native";

import Header from "./src/components/Header";
import Form from "./src/components/Form";
import CriptoInfo from "./src/components/CriptoInfo";

export default function App() {
  const [criptoInfo, setCriptoInfo] = useState({});
  const [loading, setLoading] = useState(false);

  return (
    <ScrollView>
      <Header />
      <View style={styles.container}>
        <Form setCriptoInfo={setCriptoInfo} setLoading={setLoading} />
        {loading && (
          <ActivityIndicator
            style={styles.spinner}
            size="large"
            color="#0f0623"
          />
        )}
        {!loading && Object.keys(criptoInfo).length !== 0 ? (
          <CriptoInfo criptoInfo={criptoInfo} />
        ) : null}
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: "5%",
  },
  spinner: {
    marginTop: 50,
  },
});
